fun main() {

    var f1 = Fraction(1,2)

    var f2 = Fraction(2,4)

    println(f1 == f2)
    println(f2)

}


class Fraction(private var numerator: Int, private var denominator: Int){
    override fun equals(other: Any?): Boolean {
        if(other is Fraction){
            if(numerator * other.denominator == denominator * other.numerator)
                return true
        }
        return false
    }

    override fun toString(): String {
        return ("${this.numerator} / ${this.denominator}")
    }
}